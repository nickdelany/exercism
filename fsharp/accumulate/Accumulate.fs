﻿module Accumulate

let accumulate fn data =
    
    let rec mapImp f acc = function
        | [] -> acc
        | h::t -> mapImp f (f h::acc) t
         
    mapImp fn [] data |> List.rev