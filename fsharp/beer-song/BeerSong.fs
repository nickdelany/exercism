﻿module BeerSong

let verse = function
    | 2 -> [ "2 bottles of beer on the wall, 2 bottles of beer.";
              "Take one down and pass it around, 1 bottle of beer on the wall."  ]
    | 1 -> [ "1 bottle of beer on the wall, 1 bottle of beer.";
              "Take it down and pass it around, no more bottles of beer on the wall."]
    | 0 -> [ "No more bottles of beer on the wall, no more bottles of beer.";
              "Go to the store and buy some more, 99 bottles of beer on the wall."]
    | v -> [ sprintf "%d bottles of beer on the wall, %d bottles of beer." v v;
             sprintf "Take one down and pass it around, %d bottles of beer on the wall."  (v - 1) ]

let recite (startBottles: int) (takeDown: int) =
    let ret = []

    let rec sing song bottles take =
        match take with
        | 1 -> song @ (verse bottles) 
        | _ -> sing (song @ (verse bottles) @ [""]) (bottles - 1) (take - 1) 
    
    sing ret startBottles takeDown    