﻿module Bob

open System.Text.RegularExpressions

let hey = fun speech ->
    match speech with 
    | x when Regex.Match(x, @"^[^a-z]*$").Success && Regex.Match(x, @"[A-Z]+").Success -> "Whoa, chill out!"
    | x when Regex.Match(x, @"\?$").Success -> "Sure."
    | x when Regex.Match(x, @"^ *$").Success -> "Fine. Be that way!"
    | _ -> "Whatever.";