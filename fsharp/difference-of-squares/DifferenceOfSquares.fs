﻿module DifferenceOfSquares

let square x = pown x 2

let squareOfSums = fun n ->
    [1 .. n] |> List.sum |> square;

let sumOfSquares = fun n ->
    [1 .. n] |> List.sumBy square

let difference = fun n ->
    squareOfSums n - sumOfSquares n;