﻿module Gigasecond

open System;

let gigasecond = fun (date:DateTime) ->
    date.AddSeconds(10. ** 9.).Date ;