module GradeSchool

let grade g (school:Map<string,int>) =
    school
        |> Map.filter(fun k v -> v = g)
        |> Map.toSeq
        |> Seq.map fst
        |> Seq.sort

let empty = Map.empty

let add pupil grade (school:Map<string,int>) =
    school.Add(pupil, grade)

let roster school =
   school
    |> Map.toSeq
    |> Seq.groupBy (fun (k, v) -> v)
    |> Seq.map (fun (g, p) -> (g, p |> Seq.map fst |> Seq.sort |> Seq.toList))
    |> Seq.sortBy (fun (g,p) -> g)

