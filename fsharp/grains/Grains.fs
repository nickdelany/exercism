﻿module Grains

let square n = 2I ** (n - 1)

let total = 2I ** 64 - 1I
