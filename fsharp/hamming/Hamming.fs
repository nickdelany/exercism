﻿module Hamming

let diff = fun a b -> if a = b then 0 else 1

let compute = fun a b ->
    Seq.map2 diff a b
    |> Seq.sum