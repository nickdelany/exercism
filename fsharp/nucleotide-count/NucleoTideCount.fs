﻿module NucleoTideCount

let nuc = "ATCG"

let invalid x = not (nuc |> String.exists (fun c -> c = x))

let count = fun n strand ->
    if invalid n then
        failwith "Invalid Nucleotide"
    
    strand
        |> Seq.where (fun x -> x = n)
        |> Seq.length

let nucleotideCounts = fun strand ->
    nuc
        |> Seq.map (fun x -> (x, (count x strand)))
        |> Map.ofSeq