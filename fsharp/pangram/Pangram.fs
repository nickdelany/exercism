﻿module Pangram

let lowerAlpha c =
    match c with
    | c when (c >= 'a' && c <= 'z' ) -> Some c 
    | _ -> None
                                   
let isPangram (input: string): bool =
    input.ToLower()
    |> Seq.choose lowerAlpha
    |> Seq.distinct
    |> Seq.length = 26
