module Raindrops

let drops = Map.ofList [(3, "Pling"); (5,"Plang"); (7,"Plong"); ]

let convert (n:int) =
    let dropped a i t = if (n % i) = 0 then a + t else a

    let ret =
        Map.fold dropped "" drops

    if ret = "" then
        n.ToString()
    else
        ret
