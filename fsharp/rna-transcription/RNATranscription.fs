﻿module RNATranscription

let complement = fun nucleotide ->
    match nucleotide with
    | 'G' -> 'C'
    | 'C' -> 'G'
    | 'T' -> 'A'
    | 'A' -> 'U';

let toRna = fun sequence ->
    Seq.map complement sequence