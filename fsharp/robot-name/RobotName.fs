﻿module RobotName

open System.Collections.Generic

let a2z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

let rand = System.Random()

let names = new List<string>()

type Robot = { name : string }

let mkName () =
    let letter1 =
        a2z
        |> Seq.skip (rand.Next(0, 24))
        |> Seq.head

    let letter2 =
        a2z
        |> Seq.skip (rand.Next(0, 24))
        |> Seq.head
                
    let numbers = rand.Next(0, 999)

    sprintf "%c%c%03d" letter1 letter2 numbers    

let getName() =
    let mutable keeplooking = true;
    let mutable name = ""
    let mutable count = 10
    
    while keeplooking do
        name <- mkName()
        keeplooking <- names.Contains(name)
        count <- count - 1
        if count < 0 then failwith "Ran out of names" 
        
    names.Add(name)
    
    name
    
let mkRobot() =
    { name = getName() }

let name robot =
    robot.name

let reset robot =
    let newName = mkRobot()
    
    names.Remove(robot.name) |> ignore
    
    newName
 