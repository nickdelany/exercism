﻿module SpaceAge

open System

type Planet = Mercury | Venus | Earth | Mars | Jupiter | Saturn | Uranus | Neptune

let secondsToYears = fun seconds -> Decimal.Round(seconds / (60m * 60m * 24m * 365.25m), 2)

let spaceAge = fun planet seconds ->
    seconds /
    match planet with
    | Mercury -> 0.2408467m
    | Venus -> 0.61519726m
    | Earth -> 1m
    | Mars -> 1.8808158m
    | Jupiter  -> 11.862615m
    | Saturn  -> 29.447498m
    | Uranus  -> 84.016846m
    | Neptune -> 164.79132m
    |> secondsToYears
