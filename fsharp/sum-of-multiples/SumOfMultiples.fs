﻿module SumOfMultiples

let divisibleBy = fun elements n ->
   List.exists (fun x -> n % x = 0) elements

let sumOfMultiples = fun elements upperBound ->
    let fn = divisibleBy elements
    [0..(upperBound - 1)]
        |> List.filter fn
        |> List.sum