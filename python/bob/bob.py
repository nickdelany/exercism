import re

quiet = re.compile('[a-z]+', re.UNICODE)
shout = re.compile('[A-Z]+', re.UNICODE)
ask = re.compile('.*\?\s*$', re.UNICODE)
silence = re.compile('^\s*$', re.UNICODE)


def hey(text):
    if shout.search(text) and not quiet.search(text):
        return "Whoa, chill out!"
    if ask.search(text):
        return "Sure."
    if silence.search(text):
        return "Fine. Be that way!"
    return "Whatever."
