def distance(seq1, seq2):
    if len(seq1) != len(seq2):
        raise ValueError

    ham = 0
    for i in range(0, len(seq1)):
        if seq1[i] != seq2[i]:
            ham += 1

    return ham
