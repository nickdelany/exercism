import re;

pattern = re.compile('[^a-zA-Z]+', re.UNICODE)

def is_isogram(text):
    alpha = pattern.sub('', str.lower(text))
    uniq = ''.join(set(alpha))
    return len(uniq) == len(alpha)

