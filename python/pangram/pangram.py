import re;

pattern = re.compile('[^a-zA-Z]+', re.UNICODE)

def is_pangram(text):
    alpha = len(set(pattern.sub('', str.lower(text))))
    return 26 == len(alpha)
