def translate(c):
    return {
        'G':'C',
        'C':'G',
        'T':'A',
        'A':'U'
    }[c]

def to_rna(sequence):
    try:
        return ''.join([translate(c) for c in sequence])
    except:
        pass

    return ''