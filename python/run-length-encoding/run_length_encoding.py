def decode(text):
    decoded = ''
    count = ''
    c = ''

    for c in text:
        if c.isdigit():
            count = count + c
        else:
            decoded = decode_section(count, decoded, c)
            count = ''

    return decoded

def decode_section(count, decoded, c):
    if count != '':
        for i in range(1, int(count)):
            decoded = decoded + c
    return decoded + c


def encode(text):
    encoded = ''
    ch = c = ''
    ch_count = 0

    for c in text:
        if c == ch:
            ch_count = ch_count + 1
        else:
            encoded = encode_section(ch_count, encoded, ch)
            ch_count = 1
            ch = c

    return encode_section(ch_count, encoded, ch)

def encode_section(ch_count, encoded, ch):
    if ch_count > 1:
        encoded = encoded + str(ch_count)
    encoded = encoded + ch
    return encoded
