import re;

from collections import defaultdict

pattern = re.compile('[a-z0-9]+', re.UNICODE)


def word_count(text):
    result = defaultdict(int)

    for word in pattern.findall(text.lower()):
        result[word] = result[word] + 1

    return result
